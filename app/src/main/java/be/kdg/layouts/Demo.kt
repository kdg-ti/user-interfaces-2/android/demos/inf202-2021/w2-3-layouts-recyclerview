package be.kdg.layouts

import java.time.LocalDate

data class Girl(val name:String,var age:Int) {
}

fun main (){
   var vandaag:LocalDate = LocalDate.now()
  val girls = arrayOf(Girl("Samantha",21),Girl ("Gloria",18),Girl("Vasilenya",19))
  girls.forEach({a -> a.age+=1
    println(a)})
  girls.forEach {a -> a.age+=1
    println(a)}
  girls.forEach { it.age+=1
    println(it)}
}
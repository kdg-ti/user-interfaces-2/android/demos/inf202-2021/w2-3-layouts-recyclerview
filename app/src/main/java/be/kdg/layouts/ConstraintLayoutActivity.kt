package be.kdg.layouts

import android.app.Activity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import be.kdg.layouts.adapter.StudiesAdapter
import be.kdg.layouts.databinding.ActivityConstraintLayoutBinding

class ConstraintLayoutActivity : AppCompatActivity() {

  lateinit var binding: ActivityConstraintLayoutBinding

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityConstraintLayoutBinding.inflate(layoutInflater)
    setContentView(binding.root)
    initViews()
    addEventhandlers()
  }

  private fun initViews() {
    val adapter = ArrayAdapter.createFromResource(
      this,
      R.array.courses,
      android.R.layout.simple_spinner_item
    )
    // Specify the layout to use when the list of choices appears
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    // Apply the adapter to the spinner
    binding.spinner.adapter = adapter
    binding.studiesRecycler.layoutManager=LinearLayoutManager(this)
    binding.studiesRecycler.adapter= StudiesAdapter(resources.getStringArray(R.array.courses))
  }

  private fun addEventhandlers() {
    //val closeButton = findViewById<Button>(R.id.close_button)
    binding.closeButton.setOnClickListener {
      finish()
    }
  }
}
